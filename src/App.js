import logo from './logo.svg';
import './App.css';
import InputComponent from './Component/InputComponent';

function App() {
  return (
    <div className="mx-auto px-14 h-screen overflow-auto w-screen bg-slate-700">
          <InputComponent/> 
    </div>
    
  );
}

export default App;
