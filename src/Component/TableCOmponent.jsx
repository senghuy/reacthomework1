import React, { Component } from "react";
import Swal from "sweetalert2";

export class TableCOmponent extends Component {
  display(getstd) {
    Swal.fire(
      "ID: " +
      getstd.id +
        "\nEmail: " +
        getstd.stdemail +
        "\nName: " +
        getstd.stdname +
        "\nAge: " +
        getstd.stdage +
        "\nStatus: " +
        getstd.stdstatus
    );
  }

  render() {
    return (
      <div>
        <div class="relative overflow-x-auto shadow-md sm:rounded-lg">
          <h2 className="text-2xl mt-8 mb-2 text-teal-300 font-bold">Display Student Table</h2>
          <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
            <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
              <tr>
                <th scope="col" class="px-6 py-3">
                  Student's ID
                </th>
                <th scope="col" class="px-6 py-3">
                  Student's Email
                </th>
                <th scope="col" class="px-6 py-3">
                  Student's Name
                </th>
                <th scope="col" class="px-6 py-3">
                  Student's Age
                </th>
                <th scope="col" class="px-6 py-3">
                  Student's Status
                </th>
                <th scope="col" class="px-6 py-3">
                  Action
                </th>
              </tr>
            </thead>
            <tbody>
              {this.props.data.map((newstd) => (
                <tr class="bg-white border-b odd:bg-pink-500 text-slate-50 even:bg-yellow-600  dark:bg-gray-800 dark:border-gray-700">
                  <th
                    scope="row"
                    class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white"
                  >
                    {newstd.id}
                  </th>
                  <td class="px-6 py-4">{newstd.stdemail}</td>
                  <td class="px-6 py-4">{newstd.stdname}</td>
                  <td class="px-6 py-4">{newstd.stdage}</td>
                  <td class="px-6 py-4">
                    <button
                      onClick={() => this.props.changeValue(newstd.id)}
                      type="button"
                      className={newstd.stdstatus !== "Pending"? 
                      "focus:outline-none text-white bg-green-700 hover:bg-green-800 focus:ring-4 focus:ring-green-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-green-600 dark:hover:bg-green-700 dark:focus:ring-green-800"
                      :
                      "focus:outline-none text-white bg-purple-700 hover:bg-purple-800 focus:ring-4 focus:ring-purple-300 font-medium rounded-lg text-sm px-5 py-2.5 mb-2 dark:bg-purple-600 dark:hover:bg-purple-700 dark:focus:ring-purple-900"
                    }
                    >
                      {" "}
                      {newstd.stdstatus}{" "}
                    </button>
                  </td>
                  <td class="px-6 py-4">
                    <button
                      onClick={() => this.display(newstd)}
                      type="button"
                      class="text-white bg-gradient-to-br from-purple-600 to-blue-500 hover:bg-gradient-to-bl focus:ring-4 focus:outline-none focus:ring-blue-300 dark:focus:ring-blue-800 font-medium rounded-lg text-sm px-5 py-2.5 text-center mr-2 mb-2"
                    >
                      Show
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

export default TableCOmponent;
