import React, { Component, useRef } from "react";
import TableCOmponent from "./TableCOmponent";
import Swal from "sweetalert2";

export class InputComponent extends Component {
  
  constructor() {
    super();
    this.state = {
      student: [
        {
          id: 1,
          stdemail: "run@gamil.com",
          stdname: "Run",
          stdage: 26,
          stdstatus: "Pending",
        },
        {
          id: 2,
          stdemail: "ling@gamil.com",
          stdname: "Ling",
          stdage: 22,
          stdstatus: "Done",
        },
      ],
      newStu: "",
      stdemail: "",
      stdage: "",
      stdstatus: "Pending",
      username: "",
      age: "",
      email: "",
      isNameValide: false,
      isAgeValide: false,
      isEmailValide: false,
      usernameError: "",
      emailError: "",
      ageError: "",
    };
  }

  handleNameChange = (event) => {
    this.setState({
      newStu: event.target.value,
    });
  };

  handleEmailChange = (event) => {
    this.setState({
      stdemail: event.target.value,
    });
  };

  handleAgeChange = (event) => {
    this.setState({
      stdage: event.target.value,
    });
  };

  onSubmit = (e) => {
    const newObj = {
      id: this.state.student.length + 1,
      stdemail: this.state.stdemail,
      stdname: this.state.newStu,
      stdage: this.state.stdage,
      stdstatus: this.state.stdstatus,
    };
    this.setState({
      student: [...this.state.student, newObj],
      newStu: "",
      stdage: "",
      stdemail: "",
      disabledage: true,
      disabledname: true,
      disabledemail: true,
    });
    e.preventDefault();
  };

  changeButtonValue = (id) => {
    this.state.student.map((stu) =>
      stu.id === id
        ? (stu.stdstatus = stu.stdstatus === "Pending" ? "Done" : "Pending")
        : stu.id
    );
    this.setState({ stu: this.state.student });
  };

  
  formSubmit = (e) => {
    e.preventDefault();

    var username = this.state.newStu;
    var email = this.state.stdemail;
    var age = this.state.stdage;

    // console.log(username);
    // console.log(email);

    var isNameValid = true;
    var isAgeValid = true;
    var isEmailValid = true;

    var usernameError = "";
    var emailError = "";
    var ageError = "";

    let pattName = /[^a-z]/gi;
    let pattAge = /[0-9]{3}/gi;
    let pattEmail =  /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/gi;

    // NameInvalide
    if (username === "") {
      usernameError = "Name can't be empty.";
      isNameValid = false;
    } else if (username.match(pattName) != null) {
      usernameError = "Invalid Name";
      isNameValid = false;
    } else {
      usernameError = "";
      isNameValid = true;
    }
    // EmailInvalide
    if (email === "") {
      emailError = "Email can't be empty.";
      isEmailValid = true;
    } else if (email.match(pattEmail) != null) {
      emailError = "Invalid Email";
      isEmailValid = false;
    } else {
      emailError = "";
      isEmailValid = true;
    }
    // AgeInvalide
    if (age === "") {
      ageError = "Age can't be empty.";
      isAgeValid = false;
    } else if (age.match(pattAge) != null) {
      ageError = "Age Invalidate.";
      isAgeValid = false;
    } else {
      ageError = "";
      isAgeValid = true;
    }

     if(isAgeValid && !isEmailValid && isNameValid)
    {
      const newObj = {
        id: this.state.student.length + 1,
        // stdemail: this.state.stdemail,
        // stdname: this.state.newStu,
        // stdage: this.state.stdage,
         stdemail: email,
         stdname: username,
         stdage: age,
        stdstatus: this.state.stdstatus,
      };
      this.setState({
        student: [...this.state.student, newObj],
        
      });
    }else
    {
      Swal.fire(usernameError +"\n" + emailError + 
      "\n"+ ageError)
    }
    
  };

  render() {
    return (
      <div className=" px-10 h-full ">
        <h2 className="text-sky-200 text-8xl mb-4 text-center">
          Register your information
        </h2>
        <form className="space-y-3" onSubmit={this.formSubmit}>
          <label
            for="website-admin"
            class="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
          >
            Email
          </label>
          <div class="flex">
            <span class="inline-flex items-center px-3 text-sm text-gray-900 bg-gray-200 border border-r-0 border-gray-300 rounded-l-md dark:bg-gray-600 dark:text-gray-400 dark:border-gray-600">
              <svg
                width="22px"
                height="22px"
                viewBox="0 0 24 24"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <g id="SVGRepo_bgCarrier" stroke-width="0"></g>
                <g
                  id="SVGRepo_tracerCarrier"
                  stroke-linecap="round"
                  stroke-linejoin="round"
                ></g>
                <g id="SVGRepo_iconCarrier">
                  {" "}
                  <path
                    d="M4 7.00005L10.2 11.65C11.2667 12.45 12.7333 12.45 13.8 11.65L20 7"
                    stroke="#000000"
                    stroke-width="2"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                  ></path>{" "}
                  <rect
                    x="3"
                    y="5"
                    width="18"
                    height="14"
                    rx="2"
                    stroke="#000000"
                    stroke-width="2"
                    stroke-linecap="round"
                  ></rect>{" "}
                </g>
              </svg>
            </span>
            <input
              type="text"
              id="website-admin"
              class="rounded-none rounded-r-lg bg-gray-50 border border-gray-300 text-gray-900 focus:ring-blue-500 focus:border-blue-500 block flex-1 min-w-0 w-full text-sm p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              placeholder="example@gmail.com"
              onChange={this.handleEmailChange}
            />
            <p> {this.username} </p>
          </div>

          <label
            for="website-admin"
            class="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
          >
            Name
          </label>
          <div class="flex">
            <span class="inline-flex items-center px-3 text-sm text-gray-900 bg-gray-200 border border-r-0 border-gray-300 rounded-l-md dark:bg-gray-600 dark:text-gray-400 dark:border-gray-600">
              <svg
                width="22px"
                height="22px"
                viewBox="0 0 24 24"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <g id="SVGRepo_bgCarrier" stroke-width="0"></g>
                <g
                  id="SVGRepo_tracerCarrier"
                  stroke-linecap="round"
                  stroke-linejoin="round"
                ></g>
                <g id="SVGRepo_iconCarrier">
                  {" "}
                  <path
                    d="M18 18.7023C18 15.6706 14.5 15 12 15C9.5 15 6 15.6706 6 18.7023M21 12C21 16.9706 16.9706 21 12 21C7.02944 21 3 16.9706 3 12C3 7.02944 7.02944 3 12 3C16.9706 3 21 7.02944 21 12ZM15 9C15 10.6569 13.6569 12 12 12C10.3431 12 9 10.6569 9 9C9 7.34315 10.3431 6 12 6C13.6569 6 15 7.34315 15 9Z"
                    stroke="#000000"
                    stroke-width="1.5"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                  ></path>{" "}
                </g>
              </svg>
            </span>
            <input
              type="text"
              id="website-admin"
              class="rounded-none rounded-r-lg bg-gray-50 border border-gray-300 text-gray-900 focus:ring-blue-500 focus:border-blue-500 block flex-1 min-w-0 w-full text-sm p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              placeholder="Student's name"
              onChange={this.handleNameChange}
            />
          </div>

          <label
            for="website-admin"
            class="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
          >
            Age
          </label>
          <div class="flex">
            <span class="inline-flex items-center px-3 text-sm text-gray-900 bg-gray-200 border border-r-0 border-gray-300 rounded-l-md dark:bg-gray-600 dark:text-gray-400 dark:border-gray-600">
              <svg
                width="22px"
                height="22px"
                viewBox="0 0 1024 1024"
                class="icon"
                version="1.1"
                xmlns="http://www.w3.org/2000/svg"
                fill="#000000"
              >
                <g id="SVGRepo_bgCarrier" stroke-width="0"></g>
                <g
                  id="SVGRepo_tracerCarrier"
                  stroke-linecap="round"
                  stroke-linejoin="round"
                ></g>
                <g id="SVGRepo_iconCarrier">
                  <path
                    d="M564.9 273.2V106.9c0-0.9-0.7-1.6-1.6-1.6-0.1 0-0.5 0-0.9 0.3L478.9 189c-6.9 6.9-17.7 8-25.8 2.5-29.2-19.5-63.3-29.8-98.5-29.8s-69.3 10.3-98.5 29.8c-8.1 5.4-18.9 4.4-25.8-2.5l-83.4-83.4c-0.4-0.3-0.8-0.3-0.9-0.3-0.9 0-1.6 0.7-1.6 1.6v166.3c0 12.9 1.2 25.8 3.5 38.6l62.4 8.3c11.2 1.5 19.1 11.8 17.6 23-1.3 10.3-10.1 17.7-20.2 17.7-0.9 0-1.8 0-2.7-0.2l-45-6c2.9 7.6 6.2 15 9.9 22.3l36-3c11.3-1 21.2 7.4 22.1 18.6 1 11.2-7.4 21.1-18.6 22.1l-14.8 1.2c3.8 4.9 7.9 9.8 12.1 14.4 40.5 44.3 93.1 68.7 148 68.7s107.5-24.4 148-68.7c4.2-4.6 8.3-9.4 12.1-14.4l-14.8-1.2c-11.2-1-19.6-10.9-18.6-22.1 1-11.3 10.8-19.6 22.1-18.6l36.1 3c3.7-7.3 7-14.7 9.9-22.3l-45 6c-0.9 0.1-1.8 0.2-2.7 0.2-10.1 0-18.9-7.5-20.2-17.7-1.5-11.2 6.4-21.5 17.6-23l62.4-8.3c2.1-12.8 3.3-25.7 3.3-38.6z m283.6 570.3c65.7-109.7 30-250.7-81.3-320.9L573.4 400.1c-11 20.7-24.6 40.2-40.6 57.7-48.4 52.9-111.6 82-178.2 82-26.5 0-52.6-4.6-77.3-13.6-0.2 6.7-0.3 13.5-0.1 20.3 1.5 55.7 16.9 110.5 44.7 158.4 2.7 4.7 3.3 9.9 2.2 14.8V856c0 11.3-9.2 20.4-20.4 20.4h-21.2c-11.7 0-21.3 9.5-21.3 21.2v21.3h104.2c11.3 0 20.4-9.2 20.4-20.4v-151c0-11.3 9.1-20.4 20.4-20.4 11.3 0 20.4 9.1 20.4 20.4v130.7h20.2c7.5-27.4 32.6-47.7 62.4-47.7h29.4c-6.5-19.5-9.9-40-9.9-60.6 0-104.8 85.2-190 190-190 11.3 0 20.4 9.1 20.4 20.4 0 11.3-9.2 20.4-20.4 20.4-82.2 0-149.1 66.9-149.1 149.2 0 24.9 6.3 49.5 18.1 71.3 3.5 6.3 3.3 14-0.4 20.2-3.7 6.2-10.4 10-17.6 10h-60.5c-13.1 0-23.8 10.7-23.8 23.8V919h233.3c47.2 0 92.1-22.7 120.1-60.8l9.7-14.7z m33-262.4V319.5c0-9.8-3.8-19-10.7-25.9-6.9-6.9-16.2-10.8-26-10.8-20.2 0-36.7 16.5-36.7 36.7v181.6c30.2 22.5 54.8 49.8 73.4 80z m40.9-261.6v397.8c0 0.4 0 0.8-0.1 1.2 1.2 49.8-11.3 100.3-38.8 146.2-0.1 0.2-0.3 0.5-0.4 0.7-0.1 0.1-0.1 0.2-0.2 0.2-3.3 5.7-6.9 11.2-10.8 16.4-35.6 48.8-93 77.9-153.4 77.9H485c-22.3 0-40.4-18.2-40.4-40.4v-0.5h-21.4c-8.4 23.8-31.2 40.9-57.8 40.9H259.6c-21.6 0-39.2-17.6-39.2-39.3v-22.9c0-34.3 27.9-62.1 62.1-62.1h0.8v-116c-29.1-52.4-45.3-111.7-46.9-171.9-0.4-13.5 0-26.9 1.1-40.1-22.1-12.9-42.6-29.6-61-49.7-46.3-50.7-72.9-118-72.9-184.7V106.9c0-23.4 19-42.5 42.5-42.5 10.8 0 21.1 4 28.9 11.4l0.5 0.5 72.4 72.4c32.5-18.3 69.1-27.8 106.8-27.8s74.2 9.6 106.8 27.8l72.4-72.4 0.5-0.5c7.9-7.3 18.2-11.4 28.9-11.4 23.4 0 42.5 19.1 42.5 42.5v166.3c0 30.1-5.4 60.4-15.7 89.1l177.1 111.9V319.5c0-42.8 34.8-77.6 77.6-77.6 20.7 0 40.2 8.1 54.8 22.7 14.7 14.7 22.8 34.2 22.8 54.9z"
                    fill="#663333"
                  ></path>
                  <path
                    d="M881.5 319.5v261.6c-18.5-30.2-43.2-57.5-73.4-80V319.5c0-20.2 16.5-36.7 36.7-36.7 9.8 0 19 3.8 26 10.8 6.9 6.9 10.7 16.1 10.7 25.9z"
                    fill="#B2ABAC"
                  ></path>
                  <path
                    d="M767.2 522.6c111.3 70.3 147 211.2 81.3 320.9l-9.7 14.8c-28 38-72.9 60.8-120.1 60.8H485.4v-23.8c0-13.1 10.7-23.8 23.8-23.8h60.5c7.2 0 13.9-3.8 17.6-10 3.7-6.2 3.8-13.9 0.4-20.2-11.8-21.7-18.1-46.4-18.1-71.3 0-82.3 66.9-149.2 149.1-149.2 11.3 0 20.4-9.2 20.4-20.4 0-11.3-9.2-20.4-20.4-20.4-104.8 0-190 85.2-190 190 0 20.6 3.4 41.1 9.9 60.6h-29.4c-29.8 0-54.9 20.2-62.4 47.7h-20.2V747.5c0-11.3-9.2-20.4-20.4-20.4-11.3 0-20.4 9.1-20.4 20.4v151.1c0 11.3-9.2 20.4-20.4 20.4H261.2v-21.3c0-11.7 9.5-21.2 21.3-21.2h21.2c11.3 0 20.4-9.2 20.4-20.4V719.8c1.1-4.9 0.5-10.1-2.2-14.8-27.8-47.9-43.2-102.7-44.7-158.4-0.2-6.8-0.1-13.6 0.1-20.3 24.8 8.9 50.8 13.6 77.3 13.6 66.6 0 129.8-29.1 178.2-82 16-17.5 29.6-37 40.6-57.7l193.8 122.4z"
                    fill="#B2ABAC"
                  ></path>
                  <path
                    d="M821.1 682.6c11 44.8 4.2 91.4-19.2 131.2-3.8 6.5-10.6 10.1-17.6 10.1-3.5 0-7.1-0.9-10.3-2.8-9.8-5.7-13-18.2-7.3-28 18-30.6 23.2-66.4 14.7-100.8-2.7-11 4-22 15-24.7 10.9-2.6 22 4.1 24.7 15z"
                    fill="#663333"
                  ></path>
                  <path
                    d="M564.9 106.9v166.3c0 12.9-1.2 25.8-3.5 38.6L499 320c-11.2 1.5-19.1 11.8-17.6 23 1.4 10.3 10.1 17.7 20.2 17.7 0.9 0 1.8-0.1 2.7-0.2l45-6c-2.9 7.6-6.2 15-9.9 22.3l-36.1-3c-11.3-1-21.1 7.4-22.1 18.6-0.9 11.2 7.4 21.1 18.6 22.1l14.8 1.2c-3.8 4.9-7.9 9.8-12.1 14.4-40.5 44.3-93.1 68.7-148 68.7s-107.5-24.4-148-68.7c-4.2-4.6-8.3-9.4-12.1-14.4l14.8-1.2c11.2-1 19.6-10.9 18.6-22.1-0.9-11.3-10.8-19.6-22.1-18.6l-36 3c-3.7-7.3-7-14.7-9.9-22.3l45 6c0.9 0.1 1.8 0.2 2.7 0.2 10.1 0 18.9-7.5 20.2-17.7 1.5-11.2-6.4-21.5-17.6-23l-62.4-8.3c-2.3-12.8-3.5-25.7-3.5-38.6V106.9c0-0.9 0.7-1.6 1.6-1.6 0.1 0 0.5 0 0.9 0.3l83.4 83.4c6.9 6.9 17.7 8 25.8 2.5 29.2-19.5 63.3-29.8 98.5-29.8s69.3 10.3 98.5 29.8c8.1 5.4 18.9 4.4 25.8-2.5l83.4-83.4c0.4-0.3 0.8-0.3 0.9-0.3 1.1 0 1.9 0.7 1.9 1.6z"
                    fill="#B2ABAC"
                  ></path>
                  <path
                    d="M481.4 277.9m-20 0a20 20 0 1 0 40 0 20 20 0 1 0-40 0Z"
                    fill="#663333"
                  ></path>
                  <path
                    d="M414.5 356.4c5 8.8 1.9 20.1-6.9 25.1-7.4 4.2-15.7 6.4-24 6.4-3.4 0-6.7-0.3-10-1-6.9-1.5-13.3-4.4-18.9-8.5-5.5 4.1-11.9 7-18.8 8.5-3.3 0.7-6.7 1-10 1-8.4 0-16.7-2.2-24.1-6.4-8.8-5-11.9-16.3-6.9-25.1 5-8.9 16.3-11.9 25.1-6.9 1.8 1 4.7 2.1 8.4 1.3 4.2-0.9 7.6-3.9 8.9-8 3.1-9.6 13.5-14.9 23.2-11.8 5.8 1.9 10 6.4 11.8 11.8 1.3 4 4.7 7.1 8.9 8 3.7 0.8 6.6-0.3 8.4-1.3 8.6-5 19.9-1.9 24.9 6.9z"
                    fill="#663333"
                  ></path>
                  <path
                    d="M227.8 277.9m-20 0a20 20 0 1 0 40 0 20 20 0 1 0-40 0Z"
                    fill="#663333"
                  ></path>
                </g>
              </svg>
            </span>
            <input
              type="text"
              id="website-admin"
              class="rounded-none rounded-r-lg bg-gray-50 border border-gray-300 text-gray-900 focus:ring-blue-500 focus:border-blue-500 block flex-1 min-w-0 w-full text-sm p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              placeholder="Age"
              onChange={this.handleAgeChange}
            />
          </div>
          {/* 
          <button
            disabled={
              this.state.disabledage ||
              this.state.disabledname ||
              this.state.disabledemail
            }
            onClick={this.onSubmit}
            className=" bg-teal-300 hover:bg-sky-400 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
            type="button"
          > */}

          <button
            className=" bg-teal-300 hover:bg-sky-400 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
            type="submit"
          >
            Register
          </button>
          {/* Table layout */}
          <TableCOmponent
            data={this.state.student}
            changeValue={this.changeButtonValue}
          />
        </form>
      </div>
    );
  }
}

export default InputComponent;
